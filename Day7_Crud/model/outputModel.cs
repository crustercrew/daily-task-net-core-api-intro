﻿namespace Day7_Crud.model
{
    public class outputModel
    {
        public int pk_user_id { get; set; }
        public string name { get; set; }
        public List<taskModel> tasks { get; set; }

        public outputModel()
        {
            tasks = new List<taskModel>();
        }
    }
}
