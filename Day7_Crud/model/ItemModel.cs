﻿namespace Day7_Crud.model
{
    public class ItemModel
    {
        public int Id { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public decimal Price { get; set; }
    }
}
