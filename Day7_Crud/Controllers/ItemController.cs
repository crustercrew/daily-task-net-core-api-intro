﻿using Day7_Crud.model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace Day7_Crud.Controllers
{

    //[Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly IConfiguration _connection;

        public ItemController (IConfiguration connection)
        {
            _connection = connection;
        }


        [HttpGet]
        [Route("HelloWorld")]
        public string HelloWorld()
        {
            return "Hello World";
        }
        // request get item
        [HttpGet]
        [Route("GetItems")]
        public List<ItemModel> GetItem()
        {
            List<ItemModel> items = new List<ItemModel>();
            //items = LoadItem();
            items = LoadListFromDB();
            return items;
        }

        //request get item by id
        [HttpGet]
        [Route("GetItemById")]
        public List<ItemModel> GetItemById(string itemCode)
        {
            List<ItemModel> items = new List<ItemModel>();
            items = LoadListFromDB().Where(x => x.ItemCode == itemCode).ToList();
            return items;
        }


        //Post Item
        [HttpPost]
        [Route("PostItems")]
        public string PostItem(ItemModel InputModel) 
        {
            List<ItemModel> imodel = new List<ItemModel>();
            //imodel.Add(InputModel);
            string query = "insert into tbl_items values ('" + InputModel.ItemCode + "','"+InputModel.ItemName + "','"+InputModel.Price+"')";

            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand cmd = new SqlCommand(query, connection);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }

                return "Item Added Successfully";
        }

        // membuat data static
        private List<ItemModel> LoadItem() {
            List<ItemModel> OutputList = new List<ItemModel>();

            ItemModel item = new ItemModel();

            item.Id = 1;
            item.ItemCode = "IC0001";
            item.ItemName = "Coklat";
            item.Price = 10000;
            OutputList.Add(item);

            item = new ItemModel();
            item.Id = 2;
            item.ItemCode = "IC0002";
            item.ItemName = "EsKrim";
            item.Price = 12000;
            OutputList.Add(item);

            item = new ItemModel();
            item.Id = 3;
            item.ItemCode = "IC0003";
            item.ItemName = "Chiki";
            item.Price = 7000;
            OutputList.Add(item);

            item = new ItemModel();
            item.Id = 4;
            item.ItemCode = "IC0004";
            item.ItemName = "susu";
            item.Price = 5000;
            OutputList.Add(item);

            return OutputList;
        }

        private List<ItemModel> LoadListFromDB()
        {
            List<ItemModel> List = new List<ItemModel>();
            string query = "select * from tbl_items";
            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                //connection.Open();
                SqlCommand cmd = new SqlCommand(query,connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);

                for(int i = 0; i < dt.Rows.Count; i++)
                {
                    ItemModel iModel = new ItemModel();
                    iModel.Id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    iModel.ItemCode = dt.Rows[i]["ItemCode"].ToString();
                    iModel.ItemName = dt.Rows[i]["ItemName"].ToString();
                    iModel.Price = Convert.ToDecimal(dt.Rows[i]["price"].ToString());
                    List.Add(iModel);

                }
                //connection.Close();
            }

            return List;
        }
    }
}
