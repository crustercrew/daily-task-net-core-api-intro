﻿using Day7_Crud.model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;
using System.Xml.Linq;
using System;
using System.Text.Json;

namespace Day7_Crud.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class taskController : ControllerBase
    {
        private readonly IConfiguration _connection;

        public taskController(IConfiguration connection)
        {
            _connection = connection;
        }

        public class UserTaskModel
        {
            public userModel InputModel { get; set; }
            public taskModel InputModel2 { get; set; }
        }

        [HttpPost]
        [Route("PostUserWithTask")]
        public string PostUserWithTask([FromBody] outputModel outputModel)
        {

            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                // Insert user data into the users table
                string userQuery = "INSERT INTO users (name) VALUES ('"+ outputModel.name +"'); SELECT SCOPE_IDENTITY();";
                SqlCommand userCommand = new SqlCommand(userQuery, connection);
                int userId = Convert.ToInt32(userCommand.ExecuteScalar());

                // Insert task data into the tasks table
                foreach (taskModel task in outputModel.tasks)
                {
                    string taskQuery = "INSERT INTO task (task_detail, fk_user_id) VALUES ('"+ task.task_detail + "', '"+userId+"');";
                    SqlCommand taskCommand = new SqlCommand(taskQuery, connection);
                    taskCommand.ExecuteNonQuery();
                }

                connection.Close();
            }

            return "User and tasks added successfully";
        }

        [HttpGet]
        [Route("GetUserWithTaskById")]
        public List<outputModel> GetUserWithTaskbyid(string name)
        {
            List<outputModel> userbyId = new List<outputModel>();
            userbyId = loadFromBD().Where(x => x.name == name).ToList();
            return userbyId;
        }

        [HttpGet]
        [Route("GetUserWithTask")]
        public List<outputModel> GetUserWithTask()
        {
            List<outputModel> userWithTask = new List<outputModel>();
            //items = LoadItem();
            userWithTask = loadFromBD();
            return userWithTask;
        }

        private List<outputModel> loadFromBD()
        {
            List<outputModel> outputs = new List<outputModel>();

            List<ItemModel> imodel = new List<ItemModel>();
            string queryuser = "select * from users";
            string querytask = "select * from task";
            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                //connection.Open();
                //dt users
                SqlCommand cmduser = new SqlCommand(queryuser, connection);
                SqlDataAdapter datausers = new SqlDataAdapter(cmduser);
                DataTable dtusers = new DataTable();
                datausers.Fill(dtusers);

                //datatabel task
                SqlCommand cmdtask = new SqlCommand(querytask, connection);
                SqlDataAdapter datatask = new SqlDataAdapter(cmdtask);
                DataTable dttask = new DataTable();
                datatask.Fill(dttask);

                for(int i = 0; i < dtusers.Rows.Count; i++)
                {
                    outputModel oModel = new outputModel();
                    oModel.pk_user_id = Convert.ToInt32(dtusers.Rows[i]["pk_user_id"].ToString());
                    oModel.name = dtusers.Rows[i]["name"].ToString();

                    for (int j = 0; j < dttask.Rows.Count; j++)
                    {
                        int userId = Convert.ToInt32(dttask.Rows[j]["fk_user_id"].ToString());
                        if (userId == oModel.pk_user_id)
                        {
                            taskModel tModel = new taskModel();
                            tModel.pk_task_id = Convert.ToInt32(dttask.Rows[j]["pk_task_id"].ToString());
                            tModel.task_detail = dttask.Rows[j]["task_detail"].ToString();
                            tModel.fk_user_id = userId;
                            oModel.tasks.Add(tModel);
                        }
                    }

                    outputs.Add(oModel);
                }
            }

                return outputs;
        }
    }
}
